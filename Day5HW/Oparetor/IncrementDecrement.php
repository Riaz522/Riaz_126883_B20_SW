<?php

$x = 10;


echo "Pre-increment = ";
echo ++$x;
echo "</br>";


echo "Post-increment = ";
$x++;
echo $x;
echo "</br>";


echo "Pre-Decrement = ";
echo --$x;
echo "</br>";


echo "Post-Decrement = ";
$x--;
echo $x;
echo "</br>";
